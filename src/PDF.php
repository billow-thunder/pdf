<?php
namespace Billow\Services;

use Knp\Snappy\Pdf as SnappyPdf;
use Illuminate\Support\Str;

class PDF
{
  protected $pdf;

  public function __construct()
  {
    $this->pdf = new SnappyPdf('/usr/local/bin/wkhtmltopdf');
    $this->pdf->setOption('enable-external-links', true);
    $this->pdf->setOption('username', 'user');
    $this->pdf->setOption('allow', [public_path()]);
    $this->pdf->setOption('image-quality', 100);
    $this->pdf->setOption('margin-bottom', 5);
    $this->pdf->setOption('margin-left', 8);
    $this->pdf->setOption('margin-right', 8);
    $this->pdf->setOption('margin-top', 8);
  }

  public function getDataFromView($view)
  {
    return $this->pdf->getOutputFromHtml($view);
  }

  public function download($view, $filename = null)
  {
    if (!$filename) $filename = Str::random(10);
    
    return response($this->getDataFromView($view), 200)
      ->header('Content-Type', 'Content-Type: application/pdf')
      ->header('Content-Disposition', 'attachment; filename="' . $filename . '.pdf"');
  }

  public function setOption($option, $value)
  {
    $this->pdf->setOption($option, $value);
  }

  public function setBinary($binary)
  {
    $this->pdf->setBinary($binary);
  }
}
